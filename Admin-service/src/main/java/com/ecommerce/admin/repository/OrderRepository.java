package com.ecommerce.admin.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ecommerce.admin.entity.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>{
	
	public List<Order> findAllOrdersByUserId(Long id);

}

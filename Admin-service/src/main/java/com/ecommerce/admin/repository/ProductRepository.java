package com.ecommerce.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ecommerce.admin.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}

package com.ecommerce.admin.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table (name = "cart_items")
public class CartItems {
	
	@Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column (name = "product_id")
	private Long productId;
	
	@Column (name = "quantity")
	private int quantity;
	
	@Column(name = "sub_total")
	private float subTotal;
	
	@Column (name = "user_id")
	private Long userId;
	
	@ManyToMany (mappedBy = "items")
    @JsonIgnore
    private List<Order> orders;
	
	
	public CartItems() {
		
	}

	public CartItems(long productId, int quantity, float subTotal, long userId) {
		super();
		this.productId = productId;
		this.quantity = quantity;
		this.subTotal = subTotal;
		this.userId = userId;
		}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public float getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(float totalAmount) {
		this.subTotal = totalAmount;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	

}

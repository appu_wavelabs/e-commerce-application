package com.ecommerce.admin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.admin.entity.Category;
import com.ecommerce.admin.repository.CategoryRepository;

@Service
public class CategoryService {
	
	@Autowired
	private CategoryRepository categoryRepository;

	//To add a category
	public Category addCategory(Category category) {
		return categoryRepository.save(category);
	}
	
	//TO update a category
	public Category updateCategory(Category category, Long id) {
		Category newCategory = categoryRepository.getById(id);
		newCategory.setName(category.getName());
		newCategory.setDescription(category.getDescription());
		categoryRepository.save(newCategory);
		return newCategory;
	}
	//To delete a category
	public String deleteCategory(Long id) {
		categoryRepository.deleteById(id);
		return "Category Deleted successfully";
		
	}


}

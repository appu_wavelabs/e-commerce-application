package com.ecommerce.admin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.admin.entity.Category;
import com.ecommerce.admin.header.HeaderGenerator;
import com.ecommerce.admin.service.CategoryService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class CategoryController {
	
	/** This category controller consists of api's to add, update and delete category.**/
	
	@Autowired
	private CategoryService categoryService;

	@Autowired
	private HeaderGenerator headerGenerator;
	
	//This is used to add a category
	@ApiOperation(value = "To add a category")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_CREATED, response = Category.class, message = "Added successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "wrong parameters")})
	@PostMapping("/addCategory")
	public ResponseEntity<Category> addCategory(@RequestBody Category addCategory, HttpServletRequest request) {
		Category newCategory = categoryService.addCategory(addCategory);
		if (newCategory != null) {
			return new ResponseEntity<Category>(newCategory,
					headerGenerator.getHeadersForSuccessPostMethod(request, newCategory.getId()), HttpStatus.CREATED);
		}
		return new ResponseEntity<Category>(headerGenerator.getHeadersForError(), HttpStatus.BAD_REQUEST);
	}
	
	//This is used to update a category
	@ApiOperation(value = "To update a category")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_CREATED, response = String.class, message = "updated Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, response = String.class, message = "somthing went wrong")})
	@PutMapping("/updateCategory/{id}")
	public ResponseEntity<Category> updateCategoryById(@RequestBody Category category, @PathVariable("id") Long id,
			HttpServletRequest request) {
		if (category != null)
			try {
				categoryService.updateCategory(category, id);
				return new ResponseEntity<Category>(category,
						headerGenerator.getHeadersForSuccessPostMethod(request, category.getId()), HttpStatus.CREATED);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<Category>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		return new ResponseEntity<Category>(HttpStatus.BAD_REQUEST);
	}
	
	//This is used to delete a category
	@ApiOperation(value = "To delete a category")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "deleted successfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "wrong parameters")})
	@DeleteMapping(value = "/deleteCategory/{id}", params = {"id" })
	public ResponseEntity<String> deleteCategoryById(@RequestParam("id") long id) {
		String str = categoryService.deleteCategory(id);
		if (str != null) {
			return new ResponseEntity<String>(str, headerGenerator.getHeadersForSuccessGetMethod(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);
	}

	
	


}

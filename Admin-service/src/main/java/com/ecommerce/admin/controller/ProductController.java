package com.ecommerce.admin.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.admin.entity.Product;
import com.ecommerce.admin.header.HeaderGenerator;
import com.ecommerce.admin.service.ProductService;

import io.swagger.annotations.ApiOperation;

@RestController
public class ProductController {

	@Autowired
	private ProductService productService;

	@Autowired
	private HeaderGenerator headerGenerator;

	@ApiOperation(value = "To add a product")
	@PostMapping("/addProduct")
	public ResponseEntity<Product> addProduct(@RequestBody Product product, HttpServletRequest request) {
		Product newproduct = productService.addProduct(product);
		if (newproduct != null) {
			return new ResponseEntity<Product>(newproduct,
					headerGenerator.getHeadersForSuccessPostMethod(request, newproduct.getId()), HttpStatus.CREATED);
		}
		return new ResponseEntity<Product>(headerGenerator.getHeadersForError(), HttpStatus.BAD_REQUEST);
	}
}

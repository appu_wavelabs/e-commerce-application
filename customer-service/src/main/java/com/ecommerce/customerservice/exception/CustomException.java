package com.ecommerce.customerservice.exception;

import org.springframework.http.HttpStatus;

public class CustomException extends RuntimeException {
	
	/** This exception was used for security classes while
	 * creating the jwt token **/

  private static final long serialVersionUID = 1L;

  private final String message;
  private final HttpStatus httpStatus;

  public CustomException(String message, HttpStatus httpStatus) {
    this.message = message;
    this.httpStatus = httpStatus;
  }

  @Override
  public String getMessage() {
    return message;
  }

  public HttpStatus getHttpStatus() {
    return httpStatus;
  }

}

package com.ecommerce.customerservice.validation;

@SuppressWarnings("serial")
public class EmailExistsException extends Throwable {
	
	/** THis is used when the existing email id used for new registration
	 * it will throws an exception**/

    public EmailExistsException(final String message) {
        super(message);
    }

}

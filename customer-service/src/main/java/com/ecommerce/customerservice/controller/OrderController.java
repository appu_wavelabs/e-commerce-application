package com.ecommerce.customerservice.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.customerservice.entity.CartItems;
import com.ecommerce.customerservice.entity.Order;
import com.ecommerce.customerservice.entity.Product;
import com.ecommerce.customerservice.entity.User;
import com.ecommerce.customerservice.header.HeaderGenerator;
import com.ecommerce.customerservice.repository.UserRepository;
import com.ecommerce.customerservice.service.CartService;
import com.ecommerce.customerservice.service.OrderService;
import com.ecommerce.customerservice.service.ProductService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class OrderController {
	/** In this order controller we can create an order and all the 
	 * order based api's are available here**/

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private OrderService orderService;

	@Autowired
	private CartService cartService;

	@Autowired
	private ProductService productService;

	@Autowired
	private HeaderGenerator headerGenerator;

	private Order placeOrder(User user) {
		Order order = new Order();
		List<Product> products = new ArrayList<Product>();
		List<CartItems> cartItems = cartService.getCartByUserId(user.getId());
		for (CartItems items : cartItems) {
			order.setUser(user);
			products.add(productService.getProductById(items.getProductId()));
			order.setTotalAmount(OrderService.countTotalPrice(cartItems));
			order.setDate(LocalDate.now());
			order.setTypeOfTransaction("COD");
		}
		order.setProduct(products);
		return order;
	}
	
	//it will creates an order based on specific user id
	@PostMapping(value = "/order/{userId}")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_CREATED, response = User.class, message = "Order cretaed Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, response = String.class, message = "somthing went wrong")})
	public ResponseEntity<Order> confirmOrder(@PathVariable("userId") Long userId, HttpServletRequest request) {
		User user = userRepository.getUserById(userId);
		if (user != null) {
			Order order = this.placeOrder(user);
			try {
				Order newOrder = orderService.saveOrder(order);
				orderService.updateStock(userId);
				cartService.deleteCart(newOrder.getId(), userId);
				return new ResponseEntity<Order>(order,
						headerGenerator.getHeadersForSuccessPostMethod(request, order.getId()), HttpStatus.CREATED);
			} catch (Exception ex) {
				ex.printStackTrace();
				return new ResponseEntity<Order>(headerGenerator.getHeadersForError(),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

		return new ResponseEntity<Order>(headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);
	}
	
	
	//here we can get a receipt of order by using order id
	@ApiOperation(value = "getting a order by id")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Retrived successfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "wrong parameters")})
	@GetMapping(value = "/receipt/{id}")
	public ResponseEntity<Order> getOneOrderById(@PathVariable("id") long id) {
		Order order = orderService.getOrderById(id);
		if (order != null) {
			return new ResponseEntity<Order>(order, headerGenerator.getHeadersForSuccessGetMethod(), HttpStatus.OK);
		}
		return new ResponseEntity<Order>(headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);
	}
	
	//here we can get orders of a specific user based on the user id.
	@ApiOperation(value = "getting a order by user id")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Retrived successfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "wrong parameters")})
	@GetMapping(value = "/orders/{userId}", params = "userId")
	public ResponseEntity<List<Order>> getAllOrdersByUserId(@RequestParam("userId") Long userId) {
		List<Order> products = orderService.getAllOrdersById(userId);
		if (!products.isEmpty()) {
			return new ResponseEntity<List<Order>>(products, headerGenerator.getHeadersForSuccessGetMethod(),
					HttpStatus.OK);
		}
		return new ResponseEntity<List<Order>>(headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);
	}

}
package com.ecommerce.customerservice.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.customerservice.entity.User;
import com.ecommerce.customerservice.header.HeaderGenerator;
import com.ecommerce.customerservice.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
public class UserController {
	
	/** In this user controller we have registration and login **/

	@Autowired
	private UserService userService;

	@Autowired
	private HeaderGenerator headerGenerator;
	
	 @Autowired
	  private ModelMapper modelMapper;
	 
	 @Bean
	 public ModelMapper modelMapper() {
	  return new ModelMapper();
	 }
	
	 // it registers the user based on the validated details
	@ApiOperation(value = "User Registration")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_CREATED, response = User.class, message = "User cretaed Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, response = String.class, message = "somthing went wrong")})
	@PostMapping(value = "/registration")
    public ResponseEntity<User> register(@Valid @RequestBody  User user, HttpServletRequest request){
    	if(user != null)
    		try {
    			userService.saveUser(user);
    			return new ResponseEntity<User>(
    					user,
    					headerGenerator.getHeadersForSuccessPostMethod(request, user.getId()),
    					HttpStatus.CREATED);
    		}catch (Exception e) {
    			e.printStackTrace();
    			return new ResponseEntity<User>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    	return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
    }
	
	
	// it login's thee user by using the username and password and returns the jwt token
	@ApiOperation(value="Login User By UserName and Password ")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Logged in Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "Invalid parameters")})
	@PostMapping("/login/{userName}/{password}")
	public ResponseEntity<String> loginByUserName(@PathVariable("userName") String userName,
			@PathVariable("password") String password) {
		String str = userService.signin(userName, password);
		if (!str.isEmpty()) {
			return new ResponseEntity<String>(str, headerGenerator.getHeadersForSuccessGetMethod(), HttpStatus.OK);
		}
		return new ResponseEntity<String>(headerGenerator.getHeadersForError(), HttpStatus.NOT_FOUND);
	}

	

	  @PostMapping("/signup")
	  @ApiOperation(value = "${UserController.signup}")
	 
	  public String signup(@ApiParam("Signup User") @RequestBody User user) {
	    return userService.signup(modelMapper.map(user, User.class));
	  }
	
	
	
	
}

package com.ecommerce.customerservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ecommerce.customerservice.entity.User;
import com.google.common.base.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	User getUserById(Long userId);
	
	User findByEmailId(String email);

	User findByUserName(String username);

	boolean existsByUserName(String userName);
	
	
    
}

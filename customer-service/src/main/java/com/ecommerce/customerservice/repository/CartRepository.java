package com.ecommerce.customerservice.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.ecommerce.customerservice.entity.CartItems;

@Repository
public interface CartRepository extends JpaRepository<CartItems, Long> {
	List<CartItems> findByUserId(long userId);

	@Modifying
	@Query(value = "delete FROM e_commerce.order_details_items where (order_id = :id)", nativeQuery = true)
	int deleteByIdNative(long id);

	@Modifying
	@Query(value = "delete FROM e_commerce.cart_items where (user_id = :userId)", nativeQuery = true)
	int deleteByUserIdNative(long userId);
}

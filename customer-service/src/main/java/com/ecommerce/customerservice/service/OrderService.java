package com.ecommerce.customerservice.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.customerservice.entity.CartItems;
import com.ecommerce.customerservice.entity.Order;
import com.ecommerce.customerservice.repository.OrderRepository;

@Service
@Transactional
public class OrderService {

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private CartService cartService;

	@Autowired
	private ProductService productService;

	// it will saves the order based on the user id
	public Order saveOrder(Order order) {
		return orderRepository.save(order);

	}
	
	// This will be used to get an order based on the order id
	public Order getOrderById(Long id) {
		return orderRepository.getOne(id);
	}

	// This will be used to get all orders of a specific user
	public List<Order> getAllOrdersById(Long id) {
		return orderRepository.findAllOrdersByUserId(id);
	}

	//This method is used to count the total amount of order based on product prices
	public static float countTotalPrice(List<CartItems> cart) {
		float total = 0.0f;
		for (int i = 0; i < cart.size(); i++) {
			total = total + (cart.get(i).getSubTotal());
		}
		return total;
	}
	
	//This method is used to update the product stocks after successful order
	public void updateStock(Long userId) {
		List<CartItems> cart = cartService.getCartByUserId(userId);
		for (CartItems item : cart) {
			productService.updateStock(item.getProductId(), item.getQuantity());
		}

	}
}
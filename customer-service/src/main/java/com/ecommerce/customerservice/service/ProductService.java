package com.ecommerce.customerservice.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.customerservice.entity.Product;
import com.ecommerce.customerservice.repository.ProductRepository;

@Service
@Transactional
public class ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	//THis is used to search products based on product name
	public List<Product> getAllProductsByName(String name) {
		return productRepository.findByName(name);
	}
	
	//THis is used to search products based on product category
	public List<Product> getAllProductByCategory(String categoryName) {
		return productRepository.findAllByCategoryName(categoryName);
	}
	
	//THis is used to search products based on product id
	public Product getProductById(Long id) {
		return productRepository.findById(id).orElse(null);
	}
	
	//This is used to update the product stock
	public Product updateStock(long id, int quantity) {
		Product product = productRepository.findById(id).orElse(null);
		int stock = product.getStock();
		product.setStock(stock - quantity);
		productRepository.save(product);
		return product;
		}
	
	

}

package com.ecommerce.customerservice.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.ecommerce.customerservice.config.JwtTokenProvider;
import com.ecommerce.customerservice.entity.User;
import com.ecommerce.customerservice.exception.CustomException;
import com.ecommerce.customerservice.exception.UserAlreadyExistException;
import com.ecommerce.customerservice.repository.UserRepository;
import org.springframework.security.core.AuthenticationException;


@Service
@Transactional
public class UserService {

	private final UserRepository userRepository;
	
	 @Autowired
	    private PasswordEncoder passwordEncoder;
	 
	 @Autowired
	  private JwtTokenProvider jwtTokenProvider;

	  @Autowired
	  private AuthenticationManager authenticationManager;

	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	//By using this user will be saved
	public void saveUser(User user) {
		 if (emailExists(user.getEmailId())) {
	            throw new UserAlreadyExistException("There is an account with that email address: " + user.getEmailId());
	        }
	user.setPassword(passwordEncoder.encode(user.getPassword()));
		userRepository.save(user);
	}
	
	//it will checks the entered email is already exist or not
	private boolean emailExists(final String emailId) {
        return userRepository.findByEmailId(emailId) != null;
    }
	
	//this is used to a user by using user id
	public User getUserById(Long userId) {
		return userRepository.getUserById(userId);
	}
	
	//This is used to logging in
	public String signin(String userName, String password) {
	    try {
	      authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userName, password));
	      return jwtTokenProvider.createToken(userName);
	    } catch (AuthenticationException e) {
	      throw new CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
	    }
	  }
	
	 public String signup(User user) {
		    if (!userRepository.existsByUserName(user.getUserName())) {
		      user.setPassword(passwordEncoder.encode(user.getPassword()));
		      userRepository.save(user);
		      return jwtTokenProvider.createToken(user.getUserName());
		    } else {
		      throw new CustomException("Username is already in use", HttpStatus.UNPROCESSABLE_ENTITY);
		    }
		  }

	
	
	   
	
	   
}

package com.ecommerce.customerservice.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.customerservice.entity.CartItems;
import com.ecommerce.customerservice.entity.Product;
import com.ecommerce.customerservice.repository.CartRepository;
import com.ecommerce.customerservice.repository.ProductRepository;

/*
* Cart Service Class
*/
@Service
@Transactional
public class CartService {

	@Autowired
	private CartRepository repo;

	@Autowired
	private ProductRepository productRepo;

	/*
	 * Returns list of all cart items form DB
	 */
	public List<CartItems> getAllItemsFromCart() {
		return repo.findAll();
	}

	/*
	 * This method is used to add product into cart Inputs: productId,quantity and
	 * userId
	 */
	public CartItems addItemToCart(long productId, int quantity, long userId) {
		Product product = productRepo.findById(productId).orElse(null);
		float sub_total = getSubTotalForItem(product, quantity);
		CartItems item = new CartItems(productId, quantity, sub_total, userId);
		repo.save(item);
		return item;
	}

	/*
	 * This method is used to check product exist in cart or not Inputs: productId
	 */
	public boolean checkIfItemIsExist(long productId) {
		List<CartItems> cart = getAllItemsFromCart();
		for (CartItems item : cart) {
			if (item.getProductId() == productId) {
				return true;
			}
		}
		return false;
	}

	/*
	 * This method is used to change product quantity in cart Inputs:
	 * productId,quantity and Id
	 */
	public void changeItemQuantity(long productId, Integer quantity, long id) {
		CartItems item = repo.findById(id).orElse(null);
		if (item.getProductId() == productId) {
			item.setQuantity(quantity);
		}
	}

	/*
	 * This method is used to remove product from cart Inputs: productId and userId
	 */
	public String deleteItemFromCart(long productId, long userId) {
		String str = null;
		List<CartItems> cart = getCartByUserId(userId);
		for (CartItems item : cart) {
			if (item.getProductId() == productId) {
				repo.delete(item);
				str = "Product Removed From Cart";
			} else {
				str = "Unable to Remove Product";
			}
		}
		return str;
	}

	/*
	 * Returns list of all cart items form DB of user Inputs: userId
	 */
	public List<CartItems> getCartByUserId(long userId) {
		return repo.findByUserId(userId);
	}

	/*
	 * deletes Cart by cart id
	 */
	public void deleteCart(long id, long userId) {
		remove(id);
		repo.deleteByUserIdNative(userId);
		}
	public int remove(long id) {
		return repo.deleteByIdNative(id);
	}

	/*
	 * This method is used to update cart Inputs: productId,quantity and Id
	 */
	public CartItems updateCart(long productId, int quantity, long id) {
		CartItems item = repo.findById(id).orElse(null);
		if (checkIfItemIsExist(productId)) {
			changeItemQuantity(productId, quantity, id);
			Product product = productRepo.findById(productId).orElse(null);
			item.setSubTotal(getSubTotalForItem(product, quantity));
			repo.save(item);
		}
		return item;
	}

	/*
	 * This method is used to calculate total amount of each product in cart Inputs:
	 * product instance and quantity
	 */
	public static float getSubTotalForItem(Product product, int quantity) {
		return quantity * product.getPrice();
	}

}